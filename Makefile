#    sulincix-init is an alternative init system
#    Copyright (C) 2020  sulinos / Sulin Devel
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
install:
	mkdir -p $(DESTDIR)/ || true
	cp -prfv src/* $(DESTDIR)/
	mkdir -p $(DESTDIR)/lib/init/rc{0,1,2,3,4,5,6}
	[ -f $(DESTDIR)/sbin/init ] || ln -s sulincix-init $(DESTDIR)/sbin/init
